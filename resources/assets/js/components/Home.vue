<template lang="html">
  <div class="home">
    <!-- HEADER -->
    <swiper :options="swiperOption" ref="swpHome" :style="{ 'width': windowWidth + 'px' }">
      <!-- UAN-Safe™ -->
      <swiper-slide class="slide" v-for="(slide, index) in slidesViews" :key="index" :style="{ 'background-image': 'url(' + slide.url + ')' }">
        <!-- textos y botones -->
        <div class="slide-info">
            <span class="title size-xl blue light">{{slide.title}}</span>
            <span class="title size-xl light">{{slide.subtitle}}</span>
            <p class="text size-md white light">{{slide.description}}</p>
            <div class="buttons">
              <button type="button" class="btn btn-outline-primary">
                <span class="text size-sm bold uppercase ">START NOW</span>
              </button>
              <div class="view-button">
                <div class="icon">
                  <i class="mdi mdi-play-circle text size-xl white"></i>
                </div>
                <span class="text size-sm uppercase white">VIEW VIDEO</span>
              </div>
            </div>
        </div>
        <!-- botones slide -->
      </swiper-slide>
      <div class="swiper-pagination" slot="pagination" :style="{ 'width': windowWidth + 'px' }"></div>
    </swiper>
    <!-- BLOQUE 1 (WE MAKE PEOPLE FELL BETTER) -->
    <div class="block" v-for="(block, index) in blocks" :key="index" :style="{ 'background-image': 'url(' + block.url + ')' }"  :class="block.position">
      <div class="block-info" :class="block.position">
          <span class="title size-xl light" v-if="block.title">{{block.title}}</span>
          <span class="title size-xl light">{{block.subtitle}}</span>
          <p class="text size-md white light">{{block.description}}</p>
          <div class="buttons">
            <button type="button" class="btn btn-outline-primary" :class="block.btn">
              <span class="text size-sm bold uppercase" :class="block.color">READ MORE</span>
            </button>
          </div>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  props: ['windowWidth'],
  data() {
    return {

      swiperOption: {
        initialSlide:0,
        simulateTouch: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet(index, className) {
              return `<span class="${className} border-pagination swiper-pagination-bullet-custom"><i class="mdi mdi-checkbox-blank-circle text size-sm white"></i></span>`
            }
          },
        on: {
          slideChange: function(){
          },
        },
      },
      slidesViews: [
        {
          url: '../image/home/slide.jpg',
          title: 'UAN-Safe™',
          subtitle: 'Protect your home and your family',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis felis vel diam pellentesque, et ullamcorper massa sodales. Praesent feugiat dictum fringilla.',
          route: '',
        },
        {
          url: '../image/home/slide.jpg',
          title: 'UAN-Safe™',
          subtitle: 'Protect your home and your family',
          description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis felis vel diam pellentesque, et ullamcorper massa sodales. Praesent feugiat dictum fringilla.',
          route: '',
        },
      ],
      blocks: [
        {
          url: '../image/home/bloque1.jpg',
          subtitle: 'We make people feel better',
          description: 'Locate your family and friends if you feel worried, or let them know where you are if something occurs.',
          route: '',
          color: 'white',
          btn: 'line-white',
          position: 'right'
        },
        {
          url: '../image/home/bloque2.jpg',
          title: 'Omni-System™',
          subtitle: 'A personal view on security at home, away or anywhere else',
          description: 'A flexible, portable and  smart security system that you easily install by yourself. With a wide array of sensors and and modules, it fits everywhere.',
          route: '',
          color: 'white',
          btn: 'line-white',
          position: 'left'
        },
        {
          url: '../image/home/bloque3.jpg',
          subtitle: 'UAN-Safe™ let you monitor, notify and overview ',
          description: 'Get realtime updates on emergencies and alerts in closeby areas or places you visit. Notify your networks if you need help, or just feel unsafe!',
          route: '',
          color: 'gray',
          btn: 'gray',
          position: 'right'
        },
      ],

    }

  },

  methods: {

  },
  mounted: function() {
  },
}
</script>

<style lang="scss">
@import '~@/app.scss';
.home{
  width: 100%;
  height:100%;
  .swiper-slide{
    position: relative !important;
    background-repeat: no-repeat;
    background-size: cover;
    .img-bg{
      width: auto;
      height: 100%;
      position: relative;
      img{
        width: auto;
        height: 100%;
      }
    }
  }
  // estilo info de sección
  .slide-info, .block-info{
    position: absolute;
    top: 50%;
    left: 109px;
    transform: translateY(-50%);
    width: 446px;
    display: flex;
    flex-direction: column;
    @media only screen and (max-width: 412px) {
      left: 50%;
      top: 30%;
      transform: translateX(-50%);
      width: 90%;
    }
    .buttons{
      width: 100%;
      display: flex;
      justify-content: flex-start;
      align-items: center;
      .btn-outline-primary{
        background: $white;
        background-image: none;
        border-radius: 20px;
        border: 0;
        margin-right: 15px;
        &:hover{
          background: $blue;
          .text{
            color: $white;
          }
        }
        &.line-white{
          background: transparent;
          color: $white;
          border: 2px solid white;
          &:hover{
            border: 2px solid $blue;
            .text{
              color: $blue;
            }
          }
        }
        &.gray{
          color: $darkGray;
          background: transparent;
          border: 2px solid $darkGray;
          &:hover{
            border: 2px solid $blue;
            .text{
              color: $blue;
            }
          }
          @media only screen and (max-width: 412px) {
            background: transparent;
            color: $white;
            border: 2px solid white;
            .text{
              color: $white;
            }
          }
        }
      }
      .view-button{
        height: 100%;
        display: flex;
        align-items: center;
        cursor: pointer;
        &:hover{
          .text{
            color: $blue;
          }
        }
        .icon{
          margin-right: 5px;
        }
      }
    }
  }

  // estilos paginador
  .swiper-container{
    width: 100%;
    height: 900px;
    position: relative;
    padding: 0;
    margin: 0;
    @media only screen and (max-width: 412px) {
      height: 500px;
    }
  }
  .swiper-pagination{
    width: 100%;
    height: 10%;
    display: flex;
    justify-content: center;
    align-items: center;

  }
  .swiper-pagination-bullet {
    width: 30px;
    height: 30px;
    display: inline-block;
    border-radius: 100%;
    background: transparent;
    opacity: 1;
    @media only screen and (max-width: 412px) {
      display: flex;
      justify-content: center;
      align-items: center;
    }
}
  .swiper-pagination-bullet-active {
    width: 30px;
    height: 30px;
    opacity: 1;
    background: transparent;
    border: 1px solid $white;
    display: flex;
    justify-content: center;
    align-items: center;
    .mdi-checkbox-blank-circle{
      color: $white;
    }
  }

  .block{
    width: 100%;
    height: 600px;
    position: relative;
    background-repeat: no-repeat;
    background-size: cover;
    display: flex;
    align-items: flex-start;
    @media only screen and (max-width: 412px) {
      height: 400px;
    }
    &.right{
      justify-content: flex-end;
      @media only screen and (max-device-width: 768px){
        background-position: top;
        text-shadow: 2px 2px black;
      }
    }
    &.left{
      justify-content: flex-start;
    }
    .block-info{
      position: relative;
      &.right{
        margin-right: 10%;
        @media only screen and (min-device-width: 768px){
          width: 346px;
          margin-right: 18%;
        }
      }
      &.left{
        margin-left: 10px;
      }
    }
  }
}
</style>
