require('./bootstrap');

window.Vue = require('vue');

import vuescroll from 'vuescroll';
import VueAwesomeSwiper from 'vue-awesome-swiper';
var Spinner = require('vue-spinner-component/src/Spinner.vue');

// require styles
import 'swiper/dist/css/swiper.css';
Vue.use(VueAwesomeSwiper);
Vue.use(vuescroll);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 // componentes comunes a todas las vistas
Vue.component('page-component', require('./components/PageComponent.vue'));
Vue.component('menu-component', require('./components/Menu.vue'));
Vue.component('nav-link', require('./components/NavLink.vue'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('footer-page', require('./components/Footer.vue'));

// componentes vista home
Vue.component('home-page', require('./components/Home.vue'));
// componentes vista community
Vue.component('community-page', require('./components/Community.vue'));

Vue.config.devtools = true;


const app = new Vue({
    el: '#app',
    components: {
      Spinner
    },


});
